/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ectsmonitor;

import java.util.Scanner;

/**
 *
 * @author Jeffrey de Boer (Jeffrey.de.Boer2@hva.nl)
 */
public class ECTSMonitor {

    /**
     * @param args the command line arguments
     * Deze monitor tool meet je behaalde ECTS op basis van ingevoerde cijfers
     * en berekend of je op schema ligt voor een BAS
     */
    
    //Declaratie aantal te behalen studiepunten per vak
    static final int ECTS_PROGRAMMING = 3;
    static final int ECTS_BUSINESS = 3;
    static final int ECTS_INFRASTRUCTURE = 3;
    static final int ECTS_DATABASES = 3;
    static final int ECTS_USER_INTERACTION = 3;
    static final int ECTS_PROJECT_FASTEN = 12;
    static final int ECTS_PROJECT_USR_INTERACTION = 12;
    static final int TOTAAL_ECTS = (ECTS_PROGRAMMING + ECTS_BUSINESS +
            ECTS_INFRASTRUCTURE + ECTS_DATABASES + ECTS_USER_INTERACTION +
            ECTS_PROJECT_FASTEN + ECTS_PROJECT_USR_INTERACTION);
    static final int BASWAARSCHUWING = ((ECTS_PROGRAMMING + ECTS_BUSINESS +
            ECTS_INFRASTRUCTURE + ECTS_DATABASES + ECTS_USER_INTERACTION +
            ECTS_PROJECT_FASTEN + ECTS_PROJECT_USR_INTERACTION) /6) * 5;

    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String newLine = System.lineSeparator();
    
    //Initiaal vak met studiepunten
    int programming = 0;
    int business = 0;
    int infrastructure = 0;
    int databases = 0;
    int userInteraction = 0;
    int projectFasten = 0;
    int projectUserInteraction = 0;
    
    //Initiaal behaalde ECTS
    int behaaldeECTS = 0;
        
    //invoer voor cijfers.
    System.out.print("Voer je cijfer voor Programming in ");
        double cijfer1 = input.nextDouble();
        
    System.out.print("Voer je cijfer voor Business in ");
        double cijfer2 = input.nextDouble();
        
    System.out.print("Voer je cijfer voor infrastructure in ");
        double cijfer3 = input.nextDouble();
        
    System.out.print("Voer je cijfer voor databases in ");
        double cijfer4 = input.nextDouble();
        
    System.out.print("Voer je cijfer voor User Interaction in ");
        double cijfer5 = input.nextDouble();
        
    System.out.print("Voer je cijfer voor Project Fasten your Seatbelts in ");
        double cijfer6 = input.nextDouble();
        
    System.out.print("Voer je cijfer voor Project User Interaction in ");
        double cijfer7 = input.nextDouble();
    
    //Toekenning van je ECTS als je cijfer 5.5 of hoger is.    
    if (cijfer1 >= 5.5){
    programming = ECTS_PROGRAMMING;
    behaaldeECTS += programming;
    }
    
    if (cijfer2 >= 5.5){
    business = ECTS_BUSINESS;
    behaaldeECTS += business;
    }
   
    if (cijfer3 >= 5.5){
    infrastructure = ECTS_INFRASTRUCTURE;
    behaaldeECTS += infrastructure;
    }
    
    if (cijfer4 >= 5.5){
    databases = ECTS_DATABASES;
    behaaldeECTS += databases;
    }
    
    if (cijfer5 >= 5.5){
    userInteraction = ECTS_USER_INTERACTION;
    behaaldeECTS += userInteraction; 
    }
    
    if (cijfer6 >= 5.5){
    projectFasten = ECTS_PROJECT_FASTEN;
    behaaldeECTS += projectFasten;
    }
    
    if (cijfer7 >= 5.5){
    projectUserInteraction = ECTS_PROJECT_USR_INTERACTION;
    behaaldeECTS += projectUserInteraction;
    }

    //Output cijfers en behaalde ECTS
    System.out.println("Vak: Programming" + "\t" + "Cijfer:" + cijfer1 +  "\t" +
        "ECTS Punten: " + programming);
    System.out.println("Vak: Business: " + "\t" + "Cijfer:" + cijfer2 + "\t" +
        "ECTS Punten: " + business);
    System.out.println("Vak: Infrastructure: " + "\t" + "Cijfer:" + cijfer3 +
        "\t" + "ECTS Punten: " + infrastructure);
    System.out.println("Vak: Databases: " + "\t" + "Cijfer:" + cijfer4 +
        "\t" + "ECTS Punten: " + databases);
    System.out.println("Vak: User Interaction: " + "\t" + "Cijfer:" + cijfer5 +
        "\t" + "ECTS Punten: " + userInteraction);
    System.out.println("Project: Fasten your Seatbelts: " + "\t" + "Cijfer:" +
        cijfer6 + "\t" + "ECTS Punten: " + projectFasten);
    System.out.println("Project: User Interaction: " + "\t" + "Cijfer:" +
        cijfer7 + "\t" + "ECTS Punten: " + projectUserInteraction);
  
        System.out.println(newLine); //Nieuwe regel voor netheid.
    
    //output voor je behaalde ECTS en eventuele BAS waarschuwing.
    System.out.println("Je behaalde ECTS: " + behaaldeECTS + " van de "
        + TOTAAL_ECTS);
    
    if (behaaldeECTS <= BASWAARSCHUWING){
        System.out.println("Pas op, je ligt op schema voor een BAS !");
    }
    
    };
    
    
}
